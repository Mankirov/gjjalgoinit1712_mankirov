package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh09N185.findExtraBraces;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N185Test {
    public static void main(String[] args) {
        testFindExtraBracesLeft();
        testFindExtraBracesRight();
    }

    static void testFindExtraBracesLeft() {
        assertEquals("TaskCh09N185Test.testFindExtraBracesLeft", "1 extra (", findExtraBraces("((())"));
    }

    static void testFindExtraBracesRight() {
        assertEquals("TaskCh09N185Test.testFindExtraBracesRight", "the first extra ) has an index 4", findExtraBraces("(()))"));
    }
}
