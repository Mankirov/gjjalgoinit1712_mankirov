package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh12N024.fillArraySumPreviousNumber;
import static com.getjavajob.training.algo.init.mankirovo.TaskCh12N024.fillArrayWithShift;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N024Test {
    public static void main(String[] args) {
        testFillArraySumPreviousNumber();
        testFillArrayWithShift();
    }

    static void testFillArraySumPreviousNumber() {
        assertEquals("TaskCh12N024Test.testFillArraySumPreviousNumber", new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        }, fillArraySumPreviousNumber(6));
    }

    static void testFillArrayWithShift() {
        assertEquals("TaskCh12N024Test.testFillArrayWithShift", new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        }, fillArrayWithShift(6));
    }

}
