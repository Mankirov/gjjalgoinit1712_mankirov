package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N049.recFindIndexMax;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N049Test {
    public static void main(String[] args) {
        testRecFindIndexMax();
    }

    static void testRecFindIndexMax() {
        assertEquals("TaskCh10N048Test.testRecFindMax", 4, recFindIndexMax(new int[]{1, 2, 3, 4, 5}, 5, 0, 0));
    }
}
