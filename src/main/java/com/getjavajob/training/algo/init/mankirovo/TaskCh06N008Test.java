package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh06N008.findRoots;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N008Test {
    public static void main(String[] args) {
        testFindRoots65();
        testFindRoots80();
        testFindRoots100();
    }

    static void testFindRoots65() {
        int[] expected = {1, 4, 9, 16, 25, 36, 49, 64};
        assertEquals(" TaskCh06N008Test.testFindRoots65", expected, findRoots(65));
    }

    static void testFindRoots80() {
        int[] expected = {1, 4, 9, 16, 25, 36, 49, 64};
        assertEquals(" TaskCh06N008Test.testFindRoots80", expected, findRoots(80));
    }

    static void testFindRoots100() {
        int[] expected = {1, 4, 9, 16, 25, 36, 49, 64, 81, 100};
        assertEquals(" TaskCh06N008Test.testFindRoots100", expected, findRoots(100));
    }
}
