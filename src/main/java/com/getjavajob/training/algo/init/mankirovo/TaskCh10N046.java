package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh10N046 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int scaleFactor = sc.nextInt();
        int commonRatio = sc.nextInt();
        int termNumN = sc.nextInt();
        int resultA = findMemberGeometricSequence(scaleFactor, commonRatio, termNumN);
        System.out.println(resultA);
        int resultB = findSumFirstTermsOfGeometric(scaleFactor, commonRatio, termNumN);
        System.out.println(resultB);
    }

    static int findMemberGeometricSequence(int scaleFactor, int commonRatio, int termNumN) {
        if (termNumN == 1) {
            return scaleFactor;
        }
        return findMemberGeometricSequence(scaleFactor, commonRatio, termNumN - 1) * commonRatio;
    }

    static int findSumFirstTermsOfGeometric(int scaleFactor, int commonRatio, int termNumN) {
        if (termNumN == 1) {
            return scaleFactor;
        }
        return findSumFirstTermsOfGeometric(scaleFactor * commonRatio, commonRatio, termNumN - 1) + scaleFactor;
    }
}
