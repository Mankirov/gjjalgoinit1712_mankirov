package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh09N107 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String result = swapFirstALastO(sc.nextLine());
        System.out.println(result);
    }

    static String swapFirstALastO(String sWord) {
        int a = sWord.indexOf('a');
        int b = sWord.lastIndexOf('o');
        if (a < 0 || b < 0) {
            return "In the word there are no letters A or O!";
        }
        StringBuilder sb = new StringBuilder(sWord);
        sb.setCharAt(a, 'o');
        sb.setCharAt(b, 'a');
        return sb.toString();
    }
}
