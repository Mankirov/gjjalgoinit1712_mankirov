package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh10N047 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int termNumK = sc.nextInt();
        int result = findNumFibonacci(termNumK);
        System.out.println(result);
    }

    static int findNumFibonacci(int termNumK) {
        if (termNumK > 0) {
            return recFibonacci(termNumK);
        } else if (termNumK < 0) {
            return recNegaFibonacci(termNumK);
        }
        return 0;
    }

    static int recFibonacci(int termNumK) {
        if (termNumK == 1 || termNumK == 2) {
            return 1;
        }
        return recFibonacci(termNumK - 1) + recFibonacci(termNumK - 2);
    }

    static int recNegaFibonacci(int termNumK) {
        if (termNumK == -2) {
            return -1;
        } else if (termNumK == -1) {
            return 1;
        }
        return recNegaFibonacci(termNumK + 2) - recNegaFibonacci(termNumK + 1);
    }
}
