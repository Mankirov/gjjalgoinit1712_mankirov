package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh05N010.isDollarToRuble;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N010Test {
    public static void main(String[] args) {
        testIsDollarToRuble60();
        testIsDollarToRuble58();
    }

    static void testIsDollarToRuble60() {
        double[] expected = {60.5, 121.0, 181.5, 242.0, 302.5, 363.0, 423.5, 484.0,
                544.5, 605.0, 665.5, 726.0, 786.5, 847.0, 907.5, 968.0, 1028.5, 1089.0, 1149.5, 1210.0};
        assertEquals("TaskCh05N010Test.testIsDollarToRuble", expected, isDollarToRuble(60.5));
    }

    static void testIsDollarToRuble58() {
        double[] expected = {58.5, 117.0, 175.5, 234.0, 292.5, 351.0, 409.5, 468.0,
                526.5, 585.0, 643.5, 702.0, 760.5, 819.0, 877.5, 936.0, 994.5, 1053.0, 1111.5, 1170.0};
        assertEquals("TaskCh05N010Test.testIsDollarToRuble", expected, isDollarToRuble(58.5));
    }
}
