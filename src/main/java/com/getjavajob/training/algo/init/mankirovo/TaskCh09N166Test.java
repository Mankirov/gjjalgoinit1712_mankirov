package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh09N166.swapFirstAndLastWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N166Test {
    public static void main(String[] args) {
        testSwapFirstAndLastWord();
    }

    static void testSwapFirstAndLastWord() {
        assertEquals("TaskCh09N166Test.testSwapFirstAndLastWord",
                "milk over spilt cry", swapFirstAndLastWord("cry over spilt milk"));
    }
}
