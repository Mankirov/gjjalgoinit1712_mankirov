package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh04N015 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int todayMonth = sc.nextInt();
        int todayYear = sc.nextInt();
        int birthdayMonth = sc.nextInt();
        int birthdayYear = sc.nextInt();
        int result = determineReachTheAgeOfPerson(todayMonth, todayYear, birthdayMonth, birthdayYear);
        System.out.println(result);
    }

    static int determineReachTheAgeOfPerson(int todayMonth, int todayYear, int birthdayMonth, int birthdayYear) {
        int age = todayYear - birthdayYear;
        return todayMonth >= birthdayMonth ? age : age - 1;
    }
}
