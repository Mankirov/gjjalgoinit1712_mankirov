package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh12N063.determineAverageNumberInParallels;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N063Test {
    public static void main(String[] args) {
        testDetermineAverageNumberInParallels();
    }

    static void testDetermineAverageNumberInParallels() {
        int[] expected = {22, 20, 19, 20, 20, 18, 19, 20, 17, 17, 19};
        assertEquals("TaskCh12N063Test.testDetermineAverageNumberInParallels",
                expected, determineAverageNumberInParallels(new int[][]{
                {20, 23, 22, 23},
                {20, 19, 22, 20},
                {21, 20, 20, 16},
                {19, 22, 17, 23},
                {19, 22, 22, 20},
                {15, 16, 18, 23},
                {21, 23, 16, 19},
                {23, 16, 19, 23},
                {19, 15, 16, 18},
                {19, 18, 16, 16},
                {21, 17, 23, 15},
        }));
    }
}
