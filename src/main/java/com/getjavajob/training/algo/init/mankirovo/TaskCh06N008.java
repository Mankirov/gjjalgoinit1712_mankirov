package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class TaskCh06N008 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] result = findRoots(n);
        for (int aResult : result) {
            System.out.println(aResult);
        }
    }

    static int[] findRoots(int n) {
        int[] array = new int[(int) sqrt(n)];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) pow(i + 1, 2);
        }
        return array;
    }
}
