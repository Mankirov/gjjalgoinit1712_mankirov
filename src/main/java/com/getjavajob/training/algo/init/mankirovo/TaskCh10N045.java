package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh10N045 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int firstTerm = sc.nextInt();
        int difference = sc.nextInt();
        int membNumN = sc.nextInt();
        int resultA = findMemberProgression(firstTerm, difference, membNumN);
        System.out.println(resultA);
        int resultB = findSumFirstTerms(firstTerm, difference, membNumN);
        System.out.println(resultB);
    }

    static int findMemberProgression(int member, int difference, int membNumN) {
        if (membNumN == 1) {
            return member;
        }
        return findMemberProgression(member, difference, membNumN - 1) + difference;
    }

    static int findSumFirstTerms(int term, int difference, int membNumN) {
        if (membNumN == 1) {
            return term;
        }
        return findSumFirstTerms(term, difference, membNumN - 1) + term + (membNumN - 1) * difference;
    }
}
