package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh05N010 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double dollarRate = sc.nextDouble();
        double[] result = isDollarToRuble(dollarRate);
        for (int i = 1; i <= result.length; i++) {
            System.out.println(i + " dollar(s) is " + result[i - 1] + " ruble(s)");
        }
    }

    static double[] isDollarToRuble(double dollarRate) {
        double[] rubles = new double[20];
        for (int i = 1; i <= rubles.length; i++) {
            rubles[i - 1] = i * dollarRate;
        }
        return rubles;
    }
}
