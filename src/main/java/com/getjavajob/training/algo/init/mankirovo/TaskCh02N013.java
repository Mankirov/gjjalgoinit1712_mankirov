package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh02N013 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        int result = reverseDigits(input);
        System.out.println(result);
    }

    static int reverseDigits(int input) {
        if (input < 100 || input > 999) {
            System.out.println("the number must have three digits");
            return 0;
        } else if (input >= 200) {
            System.out.println("n must be  200 > n > 100");
        }
        int firstDigit = input / 100;
        int secondDigit = input / 10 % 10;
        int thirdDigit = input % 10;
        return thirdDigit * 100 + secondDigit * 10 + firstDigit;
    }
}
