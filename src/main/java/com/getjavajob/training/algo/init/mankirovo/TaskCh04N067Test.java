package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh04N067.isWorkOrWeek;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N067Test {
    public static void main(String[] args) {
        testIsWorkOrWeekWorkday();
        testIsWorkOrWeekWeekday();
    }

    static void testIsWorkOrWeekWorkday() {
        assertEquals("TaskCh04N067Test.testIsWorkOrWeekWorkday", "Workday", isWorkOrWeek(145));
    }

    static void testIsWorkOrWeekWeekday() {
        assertEquals("TaskCh04N067Test.testIsWorkOrWeekWeekday", "Weekday", isWorkOrWeek(147));
    }
}
