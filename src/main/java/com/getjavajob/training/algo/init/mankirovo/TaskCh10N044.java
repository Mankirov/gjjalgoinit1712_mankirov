package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh10N044 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        System.out.println(getDigitRootNumber(number));
    }

    static int getDigitRootNumber(int number) {
        if (number % 10 == number) {
            return number;
        }
        return getDigitRootNumber(getSumDigitNumber(number));
    }

    static int getSumDigitNumber(int num) {
        if (num % 10 == num) {
            return num;
        }
        return getSumDigitNumber(num / 10) + num % 10;
    }
}
