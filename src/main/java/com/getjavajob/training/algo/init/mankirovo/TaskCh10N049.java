package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

import static java.lang.Math.random;

public class TaskCh10N049 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int max = sc.nextInt();
        int[] array = new int[max];
        for (int i = 0; i < max; i++) {
            array[i] = (int) (random() * max);
            System.out.print(array[i] + " ");
        }
        System.out.println();
        int result = recFindIndexMax(array, array.length, 0, 0);
        System.out.println(result);
    }

    static int recFindIndexMax(int[] array, int i, int max, int maxIndex) {
        if (i == 0) {
            return maxIndex;
        } else if (max < array[i - 1]) {
            max = array[i - 1];
            maxIndex = i - 1;
        }
        return recFindIndexMax(array, i - 1, max, maxIndex);
    }
}