package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh09N017 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean result = isSameLetter(sc.nextLine());
        System.out.println(result);
    }

    static boolean isSameLetter(String sWord) {
        char[] cWord = sWord.toCharArray();
        return cWord[0] == cWord[sWord.length() - 1];
    }
}
