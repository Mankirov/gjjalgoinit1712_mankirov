package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh09N022.isFirstHalf;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N022Test {
    public static void main(String[] args) {
        testIsFirstHalf();
        testIsFirstHalfIsError();
    }

    static void testIsFirstHalf() {
        assertEquals("TaskCh09N022Test.testIsFirstHalf", "organi", isFirstHalf("organization"));
    }

    static void testIsFirstHalfIsError() {
        assertEquals("TaskCh09N022Test.testIsFirstHalfIsError()",
                "Error! the number of letters must be even", isFirstHalf("organizer"));
    }
}
