package com.getjavajob.training.algo.init.mankirovo;

import java.util.List;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh13N012Test {
    public static void main(String[] args) {
        testGetEmployeesDate();
        testGetEmployeesName();
    }

    static void testGetEmployeesDate() {
        Database db1 = new Database();
        List employees = db1.getEmployeesDate(8);
        assertEquals("TaskCh13N012Test.testGetEmployees", 1, employees.size());
    }

    static void testGetEmployeesName() {
        Database db2 = new Database();
        List employees = db2.getEmployeesName("oleg");
        assertEquals("TaskCh13N012Test.testGetEmployeesName", 1, employees.size());
    }
}
