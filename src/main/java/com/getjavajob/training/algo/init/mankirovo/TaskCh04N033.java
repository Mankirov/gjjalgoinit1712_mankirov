package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh04N033 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        boolean resultA = isEven(x);
        System.out.println("a) " + resultA);
        boolean resultB = isOdd(x);
        System.out.println("b) " + resultB);

    }

    static boolean isEven(int x) {
        // a) Is it true that it ends with an even number?
        return x % 2 == 0;
    }

    static boolean isOdd(int x) {
        // b) Is it true that it ends with an odd digit?
        return x % 2 != 0;
    }
}
