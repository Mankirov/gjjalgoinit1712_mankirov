package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh10N043 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int resultA = calculateSumDigits(number);
        int resultB = calculateNumberDigits(number);
        System.out.println("a) calculate the sum of the digits of a natural number is " + resultA);
        System.out.println("b) calculate the number of digits of a natural number is " + resultB);
    }

    static int calculateSumDigits(int number) {
        if (number == 0) {
            return 0;
        }
        return calculateSumDigits(number / 10) + number % 10;
    }

    static int calculateNumberDigits(int number) {
        if (number == 0) {
            return 0;
        }
        return calculateNumberDigits(number / 10) + 1;
    }
}
