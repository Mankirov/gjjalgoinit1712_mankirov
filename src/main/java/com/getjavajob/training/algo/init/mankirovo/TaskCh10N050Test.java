package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N050.calculateAckermannFunc;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N050Test {
    public static void main(String[] args) {
        testCalculateAckermannFunctionsEmIsZero();
        testCalculateAckermannFunctionsEnIsZero();
        testCalculateAckermannFunctionsIsPositive();
    }

    static void testCalculateAckermannFunctionsEmIsZero() {
        assertEquals("TaskCh10N050Test.testCalculateAckermannFunctionsEmIsZero", 6, calculateAckermannFunc(0, 5));
    }

    static void testCalculateAckermannFunctionsEnIsZero() {
        assertEquals("TaskCh10N050Test.testCalculateAckermannFunctionsEnIsZero", 5, calculateAckermannFunc(3, 0));
    }

    static void testCalculateAckermannFunctionsIsPositive() {
        assertEquals("TaskCh10N050Test.testCalculateAckermannFunctionsIsPositive", 253, calculateAckermannFunc(3, 5));
    }
}
