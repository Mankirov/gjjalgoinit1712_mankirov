package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh05N038.determineCommonPath;
import static com.getjavajob.training.algo.init.mankirovo.TaskCh05N038.determineHowFar;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N038Test {
    public static void main(String[] args) {
        testDetermineHowFar();
        testDetermineCommonPath();
    }

    static void testDetermineHowFar() {
        assertEquals("TaskCh05N038Test.testDetermineHowFar", 0.688172179310195, determineHowFar());
    }

    static void testDetermineCommonPath() {
        assertEquals("TaskCh05N038Test.testDetermineCommonPath", 5.187377517639621, determineCommonPath());
    }
}
