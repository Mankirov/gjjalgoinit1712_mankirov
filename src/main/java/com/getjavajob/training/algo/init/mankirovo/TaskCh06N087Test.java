package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N087Test {
    public static void main(String[] args) {
        testScore();
        testResultWinnerIsZxc();
        testResultWinnerIsAbc();
        testResultGameIsDraw();
    }

    static void testScore() {
        Game game = new Game();
        game.team1Name = "abc";
        game.team2Name = "zxc";
        game.points1Team = 0;
        game.points2Team = 3;
        assertEquals("TaskCh06N087Test.testScore", "abc 0 : 3 zxc", game.score());
    }

    static void testResultWinnerIsZxc() {
        Game game = new Game();
        game.team1Name = "abc";
        game.team2Name = "zxc";
        game.points1Team = 0;
        game.points2Team = 3;
        String expected = "zxc is a W-I-N-N-E-R!!! \n" +
                " result of team zxc 3 points!!!\n" + "abc is a L-O-S-E-R!!! \n" + " result of team abc 0 points!!!";
        assertEquals("TaskCh06N087Test.testResultWinnerIsZxc", expected, game.result());
    }

    static void testResultWinnerIsAbc() {
        Game game = new Game();
        game.team1Name = "abc";
        game.team2Name = "zxc";
        game.points1Team = 3;
        game.points2Team = 0;
        String expected = "abc is a W-I-N-N-E-R!!! \n" + " result of team abc 3 points!!!\n" +
                "zxc is a L-O-S-E-R!!! \n" + " result of team zxc 0 points!!!";
        assertEquals("TaskCh06N087Test.testResultWinnerIsAbc", expected, game.result());
    }

    static void testResultGameIsDraw() {
        Game game = new Game();
        game.team1Name = "abc";
        game.team2Name = "zxc";
        game.points1Team = 3;
        game.points2Team = 3;
        assertEquals("TaskCh06N087Test.testResultGameIsDraw",
                "D-R-A-W!!! Teams abc and zxc scored 3 points each", game.result());
    }
}
