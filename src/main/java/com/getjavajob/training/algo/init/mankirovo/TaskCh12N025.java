package com.getjavajob.training.algo.init.mankirovo;

public class TaskCh12N025 {
    public static void main(String[] args) {
        int x = 12;
        int y = 10;
        System.out.println("a1)");
        int[][] resultA1 = a1(x, y);
        printArray(resultA1);

        System.out.println("b2)");
        int[][] resultB2 = b2(x, y);
        printArray(resultB2);

        System.out.println("v3)");
        int[][] resultV3 = v3(x, y);
        printArray(resultV3);

        System.out.println("g4)");
        int[][] resultG4 = g4(x, y);
        printArray(resultG4);

        System.out.println("d5)");
        int[][] resultD5 = d5(x, y);
        printArray(resultD5);

        System.out.println("e6)");
        int[][] resultE6 = e6(x, y);
        printArray(resultE6);

        System.out.println("je7)");
        int[][] resultJE7 = jE7(x, y);
        printArray(resultJE7);

        System.out.println("z8)");
        int[][] resultZ8 = z8(x, y);
        printArray(resultZ8);

        System.out.println("i9)");
        int[][] resultI9 = iE9(x, y);
        printArray(resultI9);

        System.out.println("k10)");
        int[][] resultK10 = k10(x, y);
        printArray(resultK10);

        System.out.println("L11)");
        int[][] resultL11 = eL11(x, y);
        printArray(resultL11);

        System.out.println("m12)");
        int[][] resultM12 = m12(x, y);
        printArray(resultM12);

        System.out.println("n13)");
        int[][] resultN13 = n13(x, y);
        printArray(resultN13);

        System.out.println("o14)");
        int[][] resultO14 = o14(x, y);
        printArray(resultO14);

        System.out.println("p15)");
        int[][] resultP15 = p15(x, y);
        printArray(resultP15);

        System.out.println("r16)");
        int[][] resultR16 = r16(x, y);
        printArray(resultR16);
    }

    static int[][] a1(int x, int y) {
        int a = 1;
        int[][] a1 = new int[x][y];
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a1[i].length; j++) {
                a1[i][j] = a++;
            }
        }
        return a1;
    }

    static int[][] b2(int x, int y) {
        int b = 1;
        int[][] b2 = new int[x][y];
        for (int j = 0; j < b2[b2.length-b2.length].length; j++) {
            for (int i = 0; i < b2.length; i++) {
                b2[i][j] = b++;
            }
        }
        return b2;
    }

    static int[][] v3(int x, int y) {
        int v = 1;
        int[][] v3 = new int[x][y];
        for (int i = 0; i < v3.length; i++) {
            for (int j = v3[i].length - 1; j >= 0; j--) {
                v3[i][j] = v++;
            }
        }
        return v3;
    }

    static int[][] g4(int x, int y) {
        int g = 1;
        int[][] g4 = new int[x][y];
        for (int j = 0; j < g4[g4.length-g4.length].length; j++) {
            for (int i = g4.length - 1; i >= 0; i--) {
                g4[i][j] = g++;
            }
        }
        return g4;
    }

    static int[][] d5(int x, int y) {
        int d = 1;
        int[][] d5 = new int[y][x];
        for (int i = 0; i < d5.length; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < d5[i].length; j++) {
                    d5[i][j] = d++;
                }
            } else {
                for (int j = d5[i].length - 1; j >= 0; j--) {
                    d5[i][j] = d++;
                }
            }
        }
        return d5;
    }

    static int[][] e6(int x, int y) {
        int e = 1;
        int[][] e6 = new int[x][y];
        for (int j = 0; j < e6[e6.length-e6.length].length; j++) {
            if (j % 2 == 0) {
                for (int i = 0; i < e6.length; i++) {
                    e6[i][j] = e++;
                }
            } else {
                for (int i = e6.length - 1; i >= 0; i--) {
                    e6[i][j] = e++;
                }
            }
        }
        return e6;
    }

    static int[][] jE7(int x, int y) {
        int je = 1;
        int[][] jE7 = new int[x][y];
        for (int i = jE7.length - 1; i >= 0; i--) {
            for (int j = 0; j < jE7[i].length; j++) {
                jE7[i][j] = je++;
            }
        }
        return jE7;
    }

    static int[][] z8(int x, int y) {
        int z = 1;
        int[][] z8 = new int[x][y];
        for (int j = z8[z8.length-z8.length].length - 1; j >= 0; j--) {
            for (int i = 0; i < z8.length; i++) {
                z8[i][j] = z++;
            }
        }
        return z8;
    }

    static int[][] iE9(int x, int y) {
        int iE = 1;
        int[][] iE9 = new int[x][y];
        for (int i = iE9.length - 1; i >= 0; i--) {
            for (int j = iE9[i].length - 1; j >= 0; j--) {
                iE9[i][j] = iE++;
            }
        }
        return iE9;
    }

    static int[][] k10(int x, int y) {
        int k = 1;
        int[][] k10 = new int[x][y];
        for (int j = k10[k10.length-k10.length].length - 1; j >= 0; j--) {
            for (int i = k10.length - 1; i >= 0; i--) {
                k10[i][j] = k++;
            }
        }
        return k10;
    }

    static int[][] eL11(int x, int y) {
        int L = 1;
        int[][] eL11 = new int[x][y];
        for (int i = eL11.length - 1; i >= 0; i--) {
            if (i % 2 != 0) {
                for (int j = 0; j < eL11[i].length; j++) {
                    eL11[i][j] = L++;
                }
            } else {
                for (int j = eL11[i].length - 1; j >= 0; j--) {
                    eL11[i][j] = L++;
                }
            }
        }
        return eL11;
    }

    static int[][] m12(int x, int y) {
        int m = 1;
        int[][] m12 = new int[x][y];
        for (int i = 0; i < m12.length; i++) {
            if (i % 2 != 0) {
                for (int j = 0; j < m12[i].length; j++) {
                    m12[i][j] = m++;
                }
            } else {
                for (int j = m12[i].length - 1; j >= 0; j--) {
                    m12[i][j] = m++;
                }
            }
        }
        return m12;
    }

    static int[][] n13(int x, int y) {
        int n = 1;
        int[][] n13 = new int[x][y];
        for (int j = n13[n13.length-n13.length].length - 1; j >= 0; j--) {
            if (j % 2 != 0) {
                for (int i = 0; i < n13.length; i++) {
                    n13[i][j] = n++;
                }
            } else {
                for (int i = n13.length - 1; i >= 0; i--) {
                    n13[i][j] = n++;
                }
            }
        }
        return n13;
    }

    static int[][] o14(int x, int y) {
        int o = 1;
        int[][] o14 = new int[x][y];
        for (int j = 0; j < o14[o14.length-o14.length].length; j++) {
            if (j % 2 != 0) {
                for (int i = 0; i < o14.length; i++) {
                    o14[i][j] = o++;
                }
            } else {
                for (int i = o14.length - 1; i >= 0; i--) {
                    o14[i][j] = o++;
                }
            }
        }
        return o14;
    }

    static int[][] p15(int x, int y) {
        int p = 1;
        int[][] p15 = new int[x][y];
        for (int i = p15.length - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j < p15[i].length; j++) {
                    p15[i][j] = p++;
                }
            } else {
                for (int j = p15[i].length - 1; j >= 0; j--) {
                    p15[i][j] = p++;
                }
            }
        }
        return p15;
    }

    static int[][] r16(int x, int y) {
        int r = 1;
        int[][] r16 = new int[x][y];
        for (int j = r16[r16.length-r16.length].length - 1; j >= 0; j--) {
            if (j % 2 == 0) {
                for (int i = 0; i < r16.length; i++) {
                    r16[i][j] = r++;
                }
            } else {
                for (int i = r16.length - 1; i >= 0; i--) {
                    r16[i][j] = r++;
                }
            }
        }
        return r16;
    }

    static void printArray(int[][] array) {
        for (int[] anArray : array) {
            for (int anAnArray : anArray) {
                System.out.print(anAnArray + " ");
            }
            System.out.println();
        }
    }
}
