package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh10N041 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long n = sc.nextInt();
        long result = recFactorial(n);
        System.out.println(result);
    }

    static long recFactorial(long n) {
        if (n == 1) {
            return 1;
        }
        return recFactorial(n - 1) * n;
    }
}
