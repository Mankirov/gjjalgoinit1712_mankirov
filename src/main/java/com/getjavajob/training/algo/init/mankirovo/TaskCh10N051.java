package com.getjavajob.training.algo.init.mankirovo;

public class TaskCh10N051 {
    public static void main(String[] args) {
        int n = 5;
        System.out.print("a)");
        recA(n);
        System.out.print("b)");
        recB(n);
        System.out.print("c)");
        recursionC(n);
        System.out.println();
    }

    static void recA(int n) {
        if (n > 0) {
            System.out.println(n);
            recA(n - 1);
        }
    }

    static void recB(int n) {
        if (n > 0) {
            recB(n - 1);
            System.out.println(n);

        }
    }

    static void recursionC(int n) {
        if (n > 0) {
            System.out.println(n);
            recursionC(n - 1);
            System.out.println(n);
        }
    }
}
