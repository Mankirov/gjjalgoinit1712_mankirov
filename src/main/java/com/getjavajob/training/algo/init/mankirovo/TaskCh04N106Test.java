package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh04N106.isSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N106Test {
    public static void main(String[] args) {
        testIsSeasonIsSpring();
        testIsSeasonIsSummer();
        testIsSeasonIsAutumn();
        testIsSeasonIsWinter();
    }

    static void testIsSeasonIsSpring() {
        assertEquals("TaskCh04N106Test.testIsSeasonIsSpring", "Spring", isSeason(5));
    }

    static void testIsSeasonIsSummer() {
        assertEquals("TaskCh04N106Test.testIsSeasonIsSummer", "Summer", isSeason(8));
    }

    static void testIsSeasonIsAutumn() {
        assertEquals("TaskCh04N106Test.testIsSeasonIsAutumn", "Autumn", isSeason(11));
    }

    static void testIsSeasonIsWinter() {
        assertEquals("TaskCh04N106Test.testIsSeasonIsWinter", "Winter", isSeason(12));
    }
}
