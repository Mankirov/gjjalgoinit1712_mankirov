package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N048.recFindMax;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N048Test {
    public static void main(String[] args) {
        testRecFindMax();
    }

    static void testRecFindMax() {
        assertEquals("TaskCh10N048Test.testRecFindMax", 5, recFindMax(new int[]{1, 2, 3, 4, 5}, 5, 0));
    }
}
