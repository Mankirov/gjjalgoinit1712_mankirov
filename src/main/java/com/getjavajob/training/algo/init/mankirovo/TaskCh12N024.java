package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh12N024 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberN = sc.nextInt();
        int[][] resultA = fillArraySumPreviousNumber(numberN);
        printArray(resultA);
        System.out.println();
        int[][] resultB = fillArrayWithShift(numberN);
        printArray(resultB);
    }

    static int[][] fillArraySumPreviousNumber(int numberN) {
        int[][] arraySumPreviousNumber = new int[numberN][numberN];
        for (int i = 0; i < arraySumPreviousNumber.length; i++) {
            for (int j = 0; j < arraySumPreviousNumber[i].length; j++) {
                if (i == 0 || j == 0) {
                    arraySumPreviousNumber[i][j] = 1;
                } else {
                    arraySumPreviousNumber[i][j] = arraySumPreviousNumber[i - 1][j] + arraySumPreviousNumber[i][j - 1];
                }
            }
        }
        return arraySumPreviousNumber;
    }

    static int[][] fillArrayWithShift(int numberN) {
        int[][] arrayWithShift = new int[numberN][numberN];
        for (int i = 0; i < arrayWithShift.length; i++) {
            for (int j = 0; j < arrayWithShift[i].length; j++) {
                arrayWithShift[i][j] = (i + j) % numberN + 1;
            }
        }
        return arrayWithShift;
    }

    static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
