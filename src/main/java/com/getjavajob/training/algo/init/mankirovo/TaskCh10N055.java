package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh10N055 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numSystem = sc.nextInt();
        int number = sc.nextInt();
        String result = "";
        System.out.println(convertDecimalToNumSys(number, result, numSystem));
    }

    static String convertDecimalToNumSys(int number, String result, int numSystem) {
        String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
        if (numSystem < 2 || numSystem > 16) {
            return "Error";
        }
        result = hex[number % numSystem] + result;
        if (number / numSystem == 0) {
            return result;
        }
        return convertDecimalToNumSys(number / numSystem, result, numSystem);
    }
}