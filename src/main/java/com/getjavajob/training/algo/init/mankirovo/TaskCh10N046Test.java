package com.getjavajob.training.algo.init.mankirovo;


import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N046.findMemberGeometricSequence;
import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N046.findSumFirstTermsOfGeometric;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N046Test {

    public static void main(String[] args) {
        testFindMemberGeometricProgression();
        testFindSumFirstTermsOfGeometricProgress();
    }

    static void testFindMemberGeometricProgression() {
        assertEquals("TaskCh10N046Test.testFindMemberGeometricProgression", 1024, findMemberGeometricSequence(2, 2, 10));
    }

    static void testFindSumFirstTermsOfGeometricProgress() {
        assertEquals("TaskCh10N046Test.testFindSumFirstTermsOfGeometricProgress", 2046, findSumFirstTermsOfGeometric(2, 2, 10));
    }
}
