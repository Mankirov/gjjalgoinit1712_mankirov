package com.getjavajob.training.algo.init.mankirovo;


import static com.getjavajob.training.algo.init.mankirovo.TaskCh04N033.isEven;
import static com.getjavajob.training.algo.init.mankirovo.TaskCh04N033.isOdd;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N033Test {
    public static void main(String[] args) {
        testIsEvenTrue();
        testIsEvenFalse();
        testIsOddTrue();
        testIsOddFalse();
    }

    static void testIsEvenTrue() {
        assertEquals("TaskCh04N033Test.testIsEvenTrue", true, isEven(10));
    }

    static void testIsEvenFalse() {
        assertEquals("TaskCh04N033Test.testIsEvenFalse", false, isEven(9));
    }

    static void testIsOddTrue() {
        assertEquals("TaskCh04N033Test.testIsOddTrue", true, isOdd(19));
    }

    static void testIsOddFalse() {
        assertEquals("TaskCh04N033Test.testIsOddFalse", false, isOdd(18));
    }
}
