package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

import static java.lang.Math.random;

public class TaskCh10N048 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int max = sc.nextInt();
        int[] array = new int[max];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (random() * max);
            System.out.print(array[i] + " ");
        }
        System.out.println();
        System.out.println(recFindMax(array, array.length, 0));
    }

    static int recFindMax(int[] array, int i, int max) {
        if (i == 0) {
            return max;
        } else if (max < array[i - 1]) {
            max = array[i - 1];
        }
        return recFindMax(array, i - 1, max);
    }
}
