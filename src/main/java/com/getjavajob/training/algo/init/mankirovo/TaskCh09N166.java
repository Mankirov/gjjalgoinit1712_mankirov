package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

import static java.lang.String.join;

public class TaskCh09N166 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String result = swapFirstAndLastWord(sc.nextLine());
        System.out.println(result);
    }

    static String swapFirstAndLastWord(String st) {
        String[] phrase = st.split(" ");
        String additionalWord = phrase[0];
        phrase[0] = phrase[phrase.length - 1];
        phrase[phrase.length - 1] = additionalWord;
        return join(" ", phrase);
    }
}
