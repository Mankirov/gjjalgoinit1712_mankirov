package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N044.getDigitRootNumber;
import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N044.getSumDigitNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N044Test {
    public static void main(String[] args) {
        testGetDigitRootNumber();
        testGetSumDigitNumber();
    }

    static void testGetDigitRootNumber() {
        assertEquals("TaskCh10N044Test.testGetDigitRootNumber", 6, getDigitRootNumber(12345));
    }

    static void testGetSumDigitNumber() {
        assertEquals("TaskCh10N044Test.testGetSumDigitNumber", 15, getSumDigitNumber(12345));
    }
}
