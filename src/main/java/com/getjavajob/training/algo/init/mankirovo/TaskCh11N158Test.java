package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh11N158.findDuplicateItems;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N158Test {
    public static void main(String[] args) {
        testFindDuplicateItems();
    }

    static void testFindDuplicateItems() {
        int[] expected = {10, 4, 2, 11, 8, 6, 0, 0, 0, 0, 0, 0};
        int[] array = {10, 4, 2, 11, 8, 4, 4, 4, 8, 6, 4, 4};
        assertEquals("testFindDuplicateItems.testFindDuplicateItems", expected, findDuplicateItems(array));
    }
}
