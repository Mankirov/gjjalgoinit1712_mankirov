package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh10N050 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        int result = calculateAckermannFunc(m, n);
        System.out.println(result);
    }

    static int calculateAckermannFunc(int m, int n) {
        if (m == 0) {
            return n + 1;
        } else if (m > 0 && n == 0) {
            return calculateAckermannFunc(m - 1, 1);
        } else if (m > 0 && n > 0) {
            return calculateAckermannFunc(m - 1, calculateAckermannFunc(m, n - 1));
        }
        System.out.println("error");
        return 0;
    }
}
