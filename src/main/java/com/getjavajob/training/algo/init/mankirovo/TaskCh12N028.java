package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh12N028 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numN = sc.nextInt();
        int[][] result = fillArraySnake(numN);
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                if (result[i][j] % 10 == result[i][j]) {
                    System.out.print(" " + result[i][j] + " ");
                } else {
                    System.out.print(result[i][j] + " ");
                }
            }
            System.out.println();
        }
    }

    static int[][] fillArraySnake(int numN) {
        int[][] arraySnake = new int[numN][numN];
        int i = 0;
        int j = 0;
        int c = numN - 1;
        int k = 0;
        for (int b = 1; b <= arraySnake.length * arraySnake.length; b++) {
            if (b - k < c + 1) {
                arraySnake[i][j++] = b;
            } else if (b - k >= c + 1 && b - k < 2 * c + 1) {
                arraySnake[i++][j] = b;
            } else if (b - k >= 2 * c + 1 && b - k < 3 * c + 1) {
                arraySnake[i][j--] = b;
            } else if (b - k >= 3 * c + 1 && b - k < 4 * c) {
                arraySnake[i--][j] = b;
            } else if (b - k == 4 * c) {
                c -= 2;
                k = b;
                arraySnake[i][j++] = b;
            } else if (b == arraySnake.length * arraySnake.length) {
                arraySnake[i][j] = b;
            }
        }
        return arraySnake;
    }
}
