package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh04N015.determineReachTheAgeOfPerson;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N015Test {
    public static void main(String[] args) {
        testDetermineReachTheAgeOfPersonCase1();
        testDetermineReachTheAgeOfPersonCase2();
        testDetermineReachTheAgeOfPersonCase3();
    }

    static void testDetermineReachTheAgeOfPersonCase1() {
        assertEquals("TaskCh04N015.testDetermineReachTheAgeOfPersonCase1", 33, determineReachTheAgeOfPerson(2, 2018, 1, 1985));
    }

    static void testDetermineReachTheAgeOfPersonCase2() {
        assertEquals("TaskCh04N015.testDetermineReachTheAgeOfPersonCase2", 33, determineReachTheAgeOfPerson(2, 2018, 2, 1985));
    }

    static void testDetermineReachTheAgeOfPersonCase3() {
        assertEquals("TaskCh04N015.testDetermineReachTheAgeOfPersonCase3", 32, determineReachTheAgeOfPerson(2, 2018, 3, 1985));
    }
}
