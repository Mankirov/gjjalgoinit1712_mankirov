package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

import static java.lang.Math.random;

public class TaskCh05N064 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double[] districtArea = isDistrictArea(sc.nextDouble());
        double[] districtPopulation = isDistrictPopulation(sc.nextDouble());
        double result = determineRegionPopulation(districtArea, districtPopulation);
        System.out.println(result);
    }

    static double[] isDistrictArea(double x) {
        double[] districtArea = new double[12];
        for (int i = 0; i < districtArea.length; i++) {
            districtArea[i] = random() * x;
            System.out.println(i + 1 + " " + districtArea[i]);
        }
        return districtArea;
    }

    static double[] isDistrictPopulation(double y) {
        double[] districtPopulation = new double[12];
        for (int i = 0; i < districtPopulation.length; i++) {
            districtPopulation[i] = random() * y;
            System.out.println(i + 1 + " " + districtPopulation[i]);
        }
        return districtPopulation;
    }

    static double determineRegionPopulation(double[] districtArea, double[] districtPopulation) {
        double sumRegionArea = 0;
        double sumRegionPopulation = 0;
        for (int i = 0; i < districtPopulation.length; i++) {
            sumRegionArea += districtArea[i];
            sumRegionPopulation += districtPopulation[i];
        }
        return sumRegionPopulation / sumRegionArea;
    }
}
