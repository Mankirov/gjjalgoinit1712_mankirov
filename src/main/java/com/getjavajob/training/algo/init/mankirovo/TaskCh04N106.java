package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh04N106 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int month = sc.nextInt();
        String result = isSeason(month);
        System.out.println(result);
    }

    static String isSeason(int month) {
        String season = "";
        switch (month % 12) {
            case 0:
            case 1:
            case 2:
                season = "Winter";
                break;
            case 3:
            case 4:
            case 5:
                season = "Spring";
                break;
            case 6:
            case 7:
            case 8:
                season = "Summer";
                break;
            case 9:
            case 10:
            case 11:
                season = "Autumn";
                break;
        }
        return season;
    }
}
