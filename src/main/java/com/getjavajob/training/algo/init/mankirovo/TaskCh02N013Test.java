package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh02N013.reverseDigits;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N013Test {
    public static void main(String[] args) {
        testReverseDigits();
    }

    static void testReverseDigits() {
        assertEquals("TaskCh02N013Test.testReverseDigits", 321, reverseDigits(123));
    }
}
