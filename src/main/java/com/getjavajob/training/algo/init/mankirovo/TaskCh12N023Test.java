package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh12N023.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N023Test {
    public static void main(String[] args) {
        testFillArrayLetterX();
        testFillArrayUnionJack();
        testFillArrayHourGlass();
    }

    static void testFillArrayLetterX() {
        assertEquals("TaskCh12N023Test.testFillArrayLetterX", new int[][]{
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        }, fillArrayLetterX(7));
    }

    static void testFillArrayUnionJack() {
        assertEquals("TaskCh12N023Test.testFillArrayUnionJack", new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        }, fillArrayUnionJack(7));
    }

    static void testFillArrayHourGlass() {
        assertEquals("TaskCh12N023Test.testFillArrayHourGlass", new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        }, fillArrayHourGlass(7));
    }
}
