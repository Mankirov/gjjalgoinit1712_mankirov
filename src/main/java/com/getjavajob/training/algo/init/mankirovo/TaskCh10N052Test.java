package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N052.recursionReverse;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N052Test {
    public static void main(String[] args) {
        testRecursionReverse();
    }

    static void testRecursionReverse() {
        assertEquals("TaskCh10N052Test.testRecursionReverse", 12345, recursionReverse(54321, 0));
    }
}
