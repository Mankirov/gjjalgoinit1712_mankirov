package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh10N042 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int power = sc.nextInt();
        int result = recCalculateNumberInPower(number, power);
        System.out.println(result);
    }

    static int recCalculateNumberInPower(int number, int power) {
        if (power == 0) {
            return 1;
        }
        return recCalculateNumberInPower(number, power - 1) * number;
    }
}
