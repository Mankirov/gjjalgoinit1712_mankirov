package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh09N015 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        int k = sc.nextInt();
        char result = findSymbolK(word, k);
        System.out.println(result);
    }

    static char findSymbolK(String word, int k) {
        char[] array = word.toCharArray();
        return array[k - 1];
    }
}
