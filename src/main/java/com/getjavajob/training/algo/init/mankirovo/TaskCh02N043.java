package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();//Two integers a and b are given.
        int result = findResiduesProduct(a, b);
        System.out.println(result);
    }

    static int findResiduesProduct(int a, int b) {
        return a % b * b % a + 1;
    }
}
