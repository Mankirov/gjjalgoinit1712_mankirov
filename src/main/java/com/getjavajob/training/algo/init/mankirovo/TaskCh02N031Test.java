package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh02N031.swapSecondAndThirdDigits;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N031Test {
    public static void main(String[] args) {
        testSwapSecondAndThirdDigits();
    }

    static void testSwapSecondAndThirdDigits() {
        assertEquals("TaskCh02N031Test.testSwapSecondAndThirdDigits", 987, swapSecondAndThirdDigits(978));
    }
}
