package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh09N042.makeReverse;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N042Test {
    public static void main(String[] args) {
        testMakeReverse();
    }

    static void testMakeReverse() {
        assertEquals("TaskCh09N042Test.testMakeReverse", "ecnarusni", makeReverse("insurance"));
    }
}
