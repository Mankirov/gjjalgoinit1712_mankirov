package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}

class Game {
    String team1Name;
    String team2Name;
    int points1Team;
    int points2Team;

    void play() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter team #1:");
        team1Name = sc.nextLine();
        System.out.println("Enter team #2:");
        team2Name = sc.nextLine();
        while (true) {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game):");
            int choice = sc.nextInt();
            if (choice == 0) {
                System.out.println(result());
                break;
            } else if (choice == 1 || choice == 2) {
                System.out.println("Enter score (1 or 2 or 3):");
                int point = sc.nextInt();
                if (point < 1 || point > 3) {
                    System.out.println("Error enter score");
                }
                if (choice == 1) {
                    //pointsAdd(points1Team, point);
                    points1Team += point;
                    System.out.println(score());
                } else {
                    //pointsAdd(points2Team, point);
                    points2Team += point;
                    System.out.println(score());
                }
            } else {
                System.out.println("Error enter team");
            }
        }
    }

    String score() {
        return team1Name + " " + points1Team + " : " + points2Team + " " + team2Name;
    }

    String result() {
        // result with the winner, the loser and their results
        if (points1Team == points2Team) {
            return "D-R-A-W!!! Teams " + team1Name + " and " + team2Name + " scored " + points2Team + " points each";
        }
        if (points1Team > points2Team) {
            return team1Name + " is a W-I-N-N-E-R!!! \n" + " result of team " + team1Name + " " + points1Team + " points!!!\n" +
                    team2Name + " is a L-O-S-E-R!!! \n" + " result of team " + team2Name + " " + points2Team + " points!!!";
        } else {
            return team2Name + " is a W-I-N-N-E-R!!! \n" + " result of team " + team2Name + " " + points2Team + " points!!!\n" +
                    team1Name + " is a L-O-S-E-R!!! \n" + " result of team " + team1Name + " " + points1Team + " points!!!";
        }
    }
}
