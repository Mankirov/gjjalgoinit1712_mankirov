package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh03N029.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh03N029Test {
    public static void main(String[] args) {
        testCheckOddNumbersCase1();
        testCheckOddNumbersCase2();
        testCheckOddNumbersCase3();
        testCheckOddNumbersCase4();
        System.out.println();
        testGetResultTaskBCase1();
        testGetResultTaskBCase2();
        testGetResultTaskBCase3();
        testGetResultTaskBCase4();
        System.out.println();
        testGetResultTaskVCase1();
        testGetResultTaskVCase2();
        testGetResultTaskVCase3();
        testGetResultTaskVCase4();
        System.out.println();
        testGetResultTaskGCase1();
        testGetResultTaskGCase2();
        testGetResultTaskGCase3();
        testGetResultTaskGCase4();
        testGetResultTaskGCase5();
        testGetResultTaskGCase6();
        testGetResultTaskGCase7();
        testGetResultTaskGCase8();
        System.out.println();
        testGetResultTaskDCase1();
        testGetResultTaskDCase2();
        testGetResultTaskDCase3();
        testGetResultTaskDCase4();
        testGetResultTaskDCase5();
        testGetResultTaskDCase6();
        testGetResultTaskDCase7();
        testGetResultTaskDCase8();
        System.out.println();
        testGetResultTaskECase1();
        testGetResultTaskECase2();
        testGetResultTaskECase3();
        testGetResultTaskECase4();
        testGetResultTaskECase5();
        testGetResultTaskECase6();
        testGetResultTaskECase7();
        testGetResultTaskECase8();
    }

    static void testCheckOddNumbersCase1() {
        assertEquals("TaskCh03N029Test.testCheckOddNumbersCase1", true, getResultTaskA(3, 5));
    }

    static void testCheckOddNumbersCase2() {
        assertEquals("TaskCh03N029Test.testCheckOddNumbersCase2", false, getResultTaskA(3, 4));
    }

    static void testCheckOddNumbersCase3() {
        assertEquals("TaskCh03N029Test.testCheckOddNumbersCase3", false, getResultTaskA(4, 5));
    }

    static void testCheckOddNumbersCase4() {
        assertEquals("TaskCh03N029Test.testCheckOddNumbersCase4", false, getResultTaskA(4, 2));
    }

    static void testGetResultTaskBCase1() {
        assertEquals("TaskCh03N029Test.testGetResultTaskBCase1", true, getResultTaskB(15, 25));
    }

    static void testGetResultTaskBCase2() {
        assertEquals("TaskCh03N029Test.testGetResultTaskBCase2", true, getResultTaskB(25, 10));
    }

    static void testGetResultTaskBCase3() {
        assertEquals("TaskCh03N029Test.testGetResultTaskBCase3", false, getResultTaskB(20, 25));
    }

    static void testGetResultTaskBCase4() {
        assertEquals("TaskCh03N029Test.testGetResultTaskBCase4", false, getResultTaskB(10, 15));
    }

    static void testGetResultTaskVCase1() {
        assertEquals("TaskCh03N029Test.testGetResultTaskVCase1", true, getResultTaskV(1, 0));
    }

    static void testGetResultTaskVCase2() {
        assertEquals("TaskCh03N029Test.testGetResultTaskVCase2", true, getResultTaskV(0, 1));
    }

    static void testGetResultTaskVCase3() {
        assertEquals("TaskCh03N029Test.testGetResultTaskVCase3", false, getResultTaskV(1, 1));
    }

    static void testGetResultTaskVCase4() {
        assertEquals("TaskCh03N029Test.testGetResultTaskVCase4", true, getResultTaskV(0, 0));
    }

    static void testGetResultTaskGCase1() {
        assertEquals("TaskCh03N029Test.testGetResultTaskGCase1", false, getResultTaskG(1, 2, 3));
    }

    static void testGetResultTaskGCase2() {
        assertEquals("TaskCh03N029Test.testGetResultTaskGCase2", false, getResultTaskG(1, 2, -3));
    }

    static void testGetResultTaskGCase3() {
        assertEquals("TaskCh03N029Test.testGetResultTaskGCase3", false, getResultTaskG(1, -2, 3));
    }

    static void testGetResultTaskGCase4() {
        assertEquals("TaskCh03N029Test.testGetResultTaskGCase4", false, getResultTaskG(1, -2, -3));
    }

    static void testGetResultTaskGCase5() {
        assertEquals("TaskCh03N029Test.testGetResultTaskGCase5", false, getResultTaskG(-1, 2, 3));
    }

    static void testGetResultTaskGCase6() {
        assertEquals("TaskCh03N029Test.testGetResultTaskGCase6", false, getResultTaskG(-1, 2, -3));
    }

    static void testGetResultTaskGCase7() {
        assertEquals("TaskCh03N029Test.testGetResultTaskGCase7", false, getResultTaskG(-1, -2, 3));
    }

    static void testGetResultTaskGCase8() {
        assertEquals("TaskCh03N029Test.testGetResultTaskGCase8", true, getResultTaskG(-1, -2, -3));
    }

    static void testGetResultTaskDCase1() {
        assertEquals("TaskCh03N029Test.testGetResultTaskDCase1", false, getResultTaskD(2, 4, 3));
    }

    static void testGetResultTaskDCase2() {
        assertEquals("TaskCh03N029Test.testGetResultTaskDCase2", true, getResultTaskD(2, 4, 30));
    }

    static void testGetResultTaskDCase3() {
        assertEquals("TaskCh03N029Test.testGetResultTaskDCase3", true, getResultTaskD(2, 40, 3));
    }

    static void testGetResultTaskDCase4() {
        assertEquals("TaskCh03N029Test.testGetResultTaskDCase4", false, getResultTaskD(2, 40, 30));
    }

    static void testGetResultTaskDCase5() {
        assertEquals("TaskCh03N029Test.testGetResultTaskDCase5", true, getResultTaskD(20, 4, 3));
    }

    static void testGetResultTaskDCase6() {
        assertEquals("TaskCh03N029Test.testGetResultTaskDCase6", false, getResultTaskD(20, 4, 30));
    }

    static void testGetResultTaskDCase7() {
        assertEquals("TaskCh03N029Test.testGetResultTaskDCase7", false, getResultTaskD(20, 40, 3));
    }

    static void testGetResultTaskDCase8() {
        assertEquals("TaskCh03N029Test.testGetResultTaskDCase8", false, getResultTaskD(20, 40, 30));
    }

    static void testGetResultTaskECase1() {
        assertEquals("TaskCh03N029Test.testGetResultTaskECase1", false, getResultTaskE(99, 99, 89));
    }

    static void testGetResultTaskECase2() {
        assertEquals("TaskCh03N029Test.testGetResultTaskECase2", true, getResultTaskE(50, 99, 189));

    }

    static void testGetResultTaskECase3() {
        assertEquals("TaskCh03N029Test.testGetResultTaskECase3", true, getResultTaskE(10, 199, 89));

    }

    static void testGetResultTaskECase4() {
        assertEquals("TaskCh03N029Test.testGetResultTaskECase4", true, getResultTaskE(8, 199, 189));

    }

    static void testGetResultTaskECase5() {
        assertEquals("TaskCh03N029Test.testGetResultTaskECase5", true, getResultTaskE(101, 99, 89));

    }

    static void testGetResultTaskECase6() {
        assertEquals("TaskCh03N029Test.testGetResultTaskECase6", true, getResultTaskE(101, 99, 189));

    }

    static void testGetResultTaskECase7() {
        assertEquals("TaskCh03N029Test.testGetResultTaskECase7", true, getResultTaskE(101, 199, 89));

    }

    static void testGetResultTaskECase8() {
        assertEquals("TaskCh03N029Test.testGetResultTaskECase8", true, getResultTaskE(101, 199, 189));
    }
}
