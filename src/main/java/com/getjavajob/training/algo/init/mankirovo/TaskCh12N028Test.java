package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh12N028.fillArraySnake;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N028Test {
    public static void main(String[] args) {
        testFillArraySnake();
    }

    static void testFillArraySnake() {
        assertEquals("TaskCh12N028Test.testFillArraySnake", new int[][]{
                        {1, 2, 3, 4, 5},
                        {16, 17, 18, 19, 6},
                        {15, 24, 25, 20, 7},
                        {14, 23, 22, 21, 8},
                        {13, 12, 11, 10, 9}
                }, fillArraySnake(5));
    }
}
