package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh02N039.determineHourHandsAngle;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N039Test {
    public static void main(String[] args) {
        testDetermineHourHandsAngleSixAM();
        testDetermineHourHandsAngleThreePM();
        testDetermineHourHandsAngleSixPM();
        testDetermineHourHandsAngleTenPM();
    }

    static void testDetermineHourHandsAngleSixAM() {
        assertEquals("TaskCh02N039Test.testDetermineHourHandsAngleSixAM", 195, determineHourHandsAngle(6, 30, 0));
    }

    static void testDetermineHourHandsAngleThreePM() {
        assertEquals("TaskCh02N039Test.testDetermineHourHandsAngleThreePM", 105, determineHourHandsAngle(15, 30, 0));
    }

    static void testDetermineHourHandsAngleSixPM() {
        assertEquals("TaskCh02N039Test.testDetermineHourHandsAngleSixPM", 165, determineHourHandsAngle(17, 30, 0));
    }

    static void testDetermineHourHandsAngleTenPM() {
        assertEquals("TaskCh02N039Test.testDetermineHourHandsAngleTenPM", 315, determineHourHandsAngle(22, 30, 0));
    }
}
