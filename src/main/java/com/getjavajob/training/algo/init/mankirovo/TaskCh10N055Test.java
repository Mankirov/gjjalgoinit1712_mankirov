package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N055.convertDecimalToNumSys;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N055Test {
    public static void main(String[] args) {
        testConvertDecimalToNumSys();
    }

    static void testConvertDecimalToNumSys() {
        assertEquals("TaskCh10N055Test.testConvertDecimalToNumSys", "7FFF", convertDecimalToNumSys(32767, "", 16));
    }
}
