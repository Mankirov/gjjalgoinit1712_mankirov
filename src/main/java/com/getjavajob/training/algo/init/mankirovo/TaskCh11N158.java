package com.getjavajob.training.algo.init.mankirovo;

import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Math.random;

public class TaskCh11N158 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (random() * n);
        }
        int[] result = findDuplicateItems(array);
        System.out.println(Arrays.toString(result));
    }

    static int[] findDuplicateItems(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int y = array.length - 1; y > i; y--) {
                if (array[i] == array[y]) {
                    removeDuplicateItem(array, y);
                }
            }
        }
        return array;
    }

    static void removeDuplicateItem(int[] array, int y) {
        for (int i = y; i < array.length - 1; i++) {
            array[i] = array[i + 1];
        }
        array[array.length - 1] = 0;
    }
}
