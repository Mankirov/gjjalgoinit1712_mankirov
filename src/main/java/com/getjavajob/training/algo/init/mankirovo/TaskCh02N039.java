package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh02N039 {
    final static double MIDDAY = 12.0;
    final static double MINUTE_PER_HOUR = 60.0;
    final static double SECONDS_PER_HOUR = 60.0 * MINUTE_PER_HOUR;
    final static double ONE_HOUR_ANGLE = 360.0 / MIDDAY;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double hours = sc.nextInt();
        double minutes = sc.nextInt();
        double seconds = sc.nextInt();
        double result = determineHourHandsAngle(hours, minutes, seconds);
        System.out.println(result);
    }

    static double determineHourHandsAngle(double hours, double minutes, double seconds) {
        return ONE_HOUR_ANGLE * (hours % MIDDAY + minutes / MINUTE_PER_HOUR + seconds / SECONDS_PER_HOUR);
    }
}
