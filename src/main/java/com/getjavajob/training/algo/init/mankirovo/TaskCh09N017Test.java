package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh09N017.isSameLetter;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N017Test {
    public static void main(String[] args) {
        testIsSameLetterIsTrue();
        testIsSameLetterIsFalse();
    }

    static void testIsSameLetterIsTrue() {
        assertEquals("TaskCh09N017Test.testIsSameLetterIsTrue", true, isSameLetter("scissors"));
    }

    static void testIsSameLetterIsFalse() {
        assertEquals("TaskCh09N017Test.testIsSameLetterIsFalse", false, isSameLetter("fabric"));
    }
}
