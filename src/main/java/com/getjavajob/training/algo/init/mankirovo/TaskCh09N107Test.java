package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh09N107.swapFirstALastO;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N107Test {
    public static void main(String[] args) {
        testSwapFirstALastO();
        testSwapFirstALastOIsErrorA();
        testSwapFirstALastOIsErrorO();
    }

    static void testSwapFirstALastO() {
        assertEquals("TaskCh09N107Test.testSwapFirstALastO", "cocaa", swapFirstALastO("cacao"));
    }

    static void testSwapFirstALastOIsErrorA() {
        assertEquals("TaskCh09N107Test.testSwapFirstALastOIsErrorA",
                "In the word there are no letters A or O!", swapFirstALastO("scissors"));
    }

    static void testSwapFirstALastOIsErrorO() {
        assertEquals("TaskCh09N107Test.testSwapFirstALastOIsErrorO",
                "In the word there are no letters A or O!", swapFirstALastO("address"));
    }
}
