package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N045.findMemberProgression;
import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N045.findSumFirstTerms;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N045Test {
    public static void main(String[] args) {
        testFindMemberProgression();
        testFindSumFirstTermsOfProgression();
    }

    static void testFindMemberProgression() {
        assertEquals("TaskCh10N045Test.testFindMemberProgression", 100, findMemberProgression(10, 10, 10));
    }

    static void testFindSumFirstTermsOfProgression() {
        assertEquals("TaskCh10N045Test.testFindSumFirstTerms", 550, findSumFirstTerms(10, 10, 10));
    }
}
