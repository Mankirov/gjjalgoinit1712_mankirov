package com.getjavajob.training.algo.init.mankirovo;

public class TaskCh05N038 {
    public static void main(String[] args) {
        double resultA = determineHowFar();
        double resultB = determineCommonPath();
        System.out.println("a) The man will be away from home  at a distance of " + resultA + " km.");
        System.out.println("b) A man will pass in this case a distance of " + resultB + " km with this.");
    }

    static double determineHowFar() {
        double sumA = 0;
        for (double i = 1; i <= 50; i++) {
            sumA += 1 / (i * 2 - 1);
            sumA -= 1 / (i * 2);
        }
        return sumA;
    }

    static double determineCommonPath() {
        double sumB = 0;
        for (double i = 1; i <= 50; i++) {
            sumB += 1 / (i * 2 - 1);
            sumB += 1 / (i * 2);
        }
        return sumB;
    }
}
