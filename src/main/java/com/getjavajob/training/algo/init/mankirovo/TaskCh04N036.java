package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh04N036 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        String result = determineColorSignal(x);
        System.out.println(result);
    }

    static String determineColorSignal(int x) {
        return x % 5 < 3 ? "Green" : "Red";
    }
}
