package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh09N015.findSymbolK;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N015Test {
    public static void main(String[] args) {
        testFindSymbolK();
    }

    static void testFindSymbolK() {
        assertEquals("TaskCh09N015Test.testFindSymbolK()", 'b', findSymbolK("symbol", 4));
    }
}
