package com.getjavajob.training.algo.init.mankirovo;

import static java.lang.Math.*;

public class TaskCh01N017 {
    final static double G = 6.67300E-11;

    public static void main(String[] args) {
        int a = 2;
        taskA(a, a);
        taskB(a, a, a);
        taskV(a, a, a);
        taskG(a, a);
        taskD(a, a);
        taskE(a, a, a);
        taskJ(a);
        taskZ(a, a, a);
        taskI(a, a, a);
        taskK(a, a);
        taskL(a, a, a);
        taskM(a, a, a);
        taskN(a, a, a, a);
        taskO(a);
        taskP(a, a, a, a);
        taskR(a);
        taskS(a);
        taskT(a);
    }

    static double taskA(int x1, int x2) {
        return sqrt(pow(x1, 2) + pow(x2, 2));
    }

    static int taskB(int x1, int x2, int x3) {
        return x1 * x2 + x1 * x3 + x2 * x3;
    }

    static double taskV(int v0, int t, int a) {
        return v0 * t + a * pow(t, 2) / 2;
    }

    static double taskG(int m, int v) {
        return m * pow(v, 2);
    }

    static double taskD(int r1, int r2) {
        return 1 / r1 + 1 / r2;
    }

    static double taskE(int m, int g, int a) {
        return m * g * cos(a);
    }

    static double taskJ(int r) {
        return 2 * PI * r;
    }

    static double taskZ(int b, int a, int c) {
        return pow(b, 2) + 4 * a * c;
    }

    static double taskI(int m1, int m2, int r) {

        return G * m1 * m2 / pow(r, 2);
    }

    static double taskK(int i, int r) {
        return pow(i, 2) * r;
    }

    static double taskL(int a, int b, int c) {
        return a * b * sin(c);
    }

    static double taskM(int a, int b, int c) {
        return sqrt(pow(a, 2) + pow(b, 2) - 2 * a * b * cos(c));
    }

    static double taskN(int a, int b, int c, int d) {
        return (a * d + b * c) / (a * d);
    }

    static double taskO(int x) {
        return sqrt(1 - pow(sin(x), 2));
    }

    static double taskP(int a, int x, int b, int c) {
        return 1 / (sqrt(a * pow(x, 2)) + b * x + c);
    }

    static double taskR(int x) {
        return (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x));
    }

    static int taskS(int x) {
        return abs(x) + abs(x + 1);
    }

    static int taskT(int x) {
        return abs(1 - abs(x));
    }
}
