package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh01N003 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        System.out.println("You entered a number " + number);
    }
}
