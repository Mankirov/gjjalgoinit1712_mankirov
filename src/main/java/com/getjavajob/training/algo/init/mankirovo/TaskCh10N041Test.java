package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N041.recFactorial;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N041Test {
    public static void main(String[] args) {
        testRecFactorialNumber20();
        testRecFactorialNumber7();
    }

    static void testRecFactorialNumber20() {
        assertEquals("TaskCh10N041Test.testRecFactorialNumber20", 2432902008176640000L, recFactorial(20));
    }

    static void testRecFactorialNumber7() {
        assertEquals("TaskCh10N041Test.testRecFactorialNumber7", 5040, recFactorial(7));
    }
}
