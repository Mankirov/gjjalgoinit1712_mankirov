package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh11N245.separateNegativeAndPositive;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N245Test {
    public static void main(String[] args) {
        testSeparateNegativeAndPositiveCase1();
        testSeparateNegativeAndPositiveCase2();
    }

    static void testSeparateNegativeAndPositiveCase1() {
        int[] expected = {-10, -2, -8, -4, -8, -4, -6, 4, 11, 4, 4, 4};
        int[] oldArray = {-10, 4, -2, 11, -8, 4, -4, 4, -8, 4, -4, -6};
        assertEquals("TaskCh11N245Test.testSeparateNegativeAndPositiveCase1", expected, separateNegativeAndPositive(oldArray));
    }

    static void testSeparateNegativeAndPositiveCase2() {
        int[] expected = {-2, -3, -4, 2, 1, 0, 2, 2, 0, 3, 2, 0};
        int[] oldArray = {2, 1, 0, 2, -2, 2, 0, 3, -3, -4, 2, 0};
        assertEquals("TaskCh11N245Test.testSeparateNegativeAndPositiveCase2", expected, separateNegativeAndPositive(oldArray));
    }
}