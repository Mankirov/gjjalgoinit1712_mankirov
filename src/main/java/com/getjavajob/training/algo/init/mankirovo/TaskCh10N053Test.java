package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N053.recReverseArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N053Test {
    public static void main(String[] args) {
        testRecReverseArray();
    }

    static void testRecReverseArray() {
        int[] expected = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        assertEquals("TaskCh10N053Test.testRecReverseArray", expected,recReverseArray(array, 10));
    }
}
