package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh04N115.isAnimalAndColor;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N115Test {
    public static void main(String[] args) {
        testIsAnimalAndColor1989();
        testIsAnimalAndColor1985();
    }

    static void testIsAnimalAndColor1989() {
        assertEquals("TaskCh04N115Test.testIsAnimalAndColor1989", "Snake, Yellow-Ground", isAnimalAndColor(1989));
    }

    static void testIsAnimalAndColor1985() {
        assertEquals("TaskCh04N115Test.testIsAnimalAndColor1985", "Bull, Tree-Green", isAnimalAndColor(1985));
    }
}
