package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N047.findNumFibonacci;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N047Test {
    public static void main(String[] args) {
        testFindNumFibonacciPositive();
        testFindNumFibonacciNegative();
        testFindNumFibonacciZero();
    }

    static void testFindNumFibonacciPositive() {
        assertEquals("TaskCh10N048Test.testFindNumFibonacciPositive", 34, findNumFibonacci(9));
    }

    static void testFindNumFibonacciNegative() {
        assertEquals("TaskCh10N048Test.testFindNumFibonacciNegative", -55, findNumFibonacci(-10));
    }

    static void testFindNumFibonacciZero() {
        assertEquals("TaskCh10N048Test.testFindNumFibonacciZero", 0, findNumFibonacci(0));
    }
}
