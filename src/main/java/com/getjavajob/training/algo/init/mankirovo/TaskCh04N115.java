package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh04N115 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();
        String result = isAnimalAndColor(year);
        System.out.println(result);
    }

    static String isAnimalAndColor(int year) {
        String animal = "";
        switch (year % 12) {
            case 0:
                animal = "Monkey";
                break;
            case 1:
                animal = "Rooster";
                break;
            case 2:
                animal = "Dog";
                break;
            case 3:
                animal = "Pig";
                break;
            case 4:
                animal = "Rat";
                break;
            case 5:
                animal = "Bull";
                break;
            case 6:
                animal = "Tiger";
                break;
            case 7:
                animal = "Rabbit";
                break;
            case 8:
                animal = "Dragon";
                break;
            case 9:
                animal = "Snake";
                break;
            case 10:
                animal = "Horse";
                break;
            case 11:
                animal = "Sheep";
                break;
        }
        String color = "";
        switch (year % 10) {
            case 0:
            case 1:
                color = "White-Metal";
                break;
            case 2:
            case 3:
                color = "Black-Water";
                break;
            case 4:
            case 5:
                color = "Tree-Green";
                break;
            case 6:
            case 7:
                color = "Red-Fire";
                break;
            case 8:
            case 9:
                color = "Yellow-Ground";
                break;
        }
        return animal + ", " + color;
    }
}
