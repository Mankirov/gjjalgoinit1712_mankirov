package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh12N023 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberN = sc.nextInt();
        if (numberN % 2 == 0) {
            System.out.println("error");
        }
        int[][] result1 = fillArrayLetterX(numberN);
        printArray(result1);
        System.out.println();
        int[][] result2 = fillArrayUnionJack(numberN);
        printArray(result2);
        System.out.println();
        int[][] result3 = fillArrayHourGlass(numberN);
        printArray(result3);
    }

    static int[][] fillArrayLetterX(int numberN) {
        int[][] arrayLetterX = new int[numberN][numberN];
        for (int i = 0; i < arrayLetterX.length; i++) {
            for (int y = 0; y < arrayLetterX[i].length; y++) {
                if (i == y || i == numberN - y - 1) {
                    arrayLetterX[i][y] = 1;
                }
            }
        }
        return arrayLetterX;
    }

    static int[][] fillArrayUnionJack(int numberN) {
        int[][] arrayUnionJack = new int[numberN][numberN];
        for (int i = 0; i < arrayUnionJack.length; i++) {
            for (int y = 0; y < arrayUnionJack[i].length; y++) {
                if (i == y || i == numberN - y - 1 || i == numberN / 2 || y == numberN / 2) {
                    arrayUnionJack[i][y] = 1;
                }
            }
        }
        return arrayUnionJack;
    }

    static int[][] fillArrayHourGlass(int numberN) {
        int[][] arrayHourGlass = new int[numberN][numberN];
        for (int i = 0; i < arrayHourGlass.length; i++) {
            for (int y = 0; y < arrayHourGlass[i].length; y++) {
                if (i >= y && i >= numberN - y - 1 || i <= y && i <= numberN - y - 1) {
                    arrayHourGlass[i][y] = 1;
                }
            }
        }
        return arrayHourGlass;
    }

    static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
