package com.getjavajob.training.algo.init.mankirovo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaskCh13N012 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        int n = sc.nextInt();
        System.out.println(" ");
        Database db = new Database();
        List<Employee> result = db.getEmployeesDate(n);
        System.out.println("Surname, Name, Patronymic and Address of employees who have worked for at least " + n + " years");
        for (Employee E : result) {
            System.out.println(E.getSurname() + " " + E.getName() + " " + E.getPatronymic() + " " + E.getAddress() + " "
                    + E.getMonthOfEmployment() + " " + E.getYearOfEmployment() + " " + E.getWorkExperience());
        }
        List<Employee> result2 = db.getEmployeesName(s);
        System.out.println("\n Employees found");
        for (Employee E : result2) {
            System.out.println(E.getSurname() + " " + E.getName() + " " + E.getPatronymic() + " "
                    + E.getMonthOfEmployment() + " " + E.getYearOfEmployment() + " " + E.getWorkExperience());
        }
    }
}

class Employee {
    private final int monthOfEmployment;
    private final int yearOfEmployment;
    private final String surname;
    private final String name;
    private final String address;
    private final String patronymic;

    public Employee(String surname, String name, String patronymic,
                    String address, int monthOfEmployment, int yearOfEmployment) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
        this.monthOfEmployment = monthOfEmployment;
        this.yearOfEmployment = yearOfEmployment;
    }

    public Employee(String surname, String name, String address, int monthOfEmployment, int employmentDateYear) {
        this(surname, name, "", address, monthOfEmployment, employmentDateYear);
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic != null ? patronymic : "";
    }

    public String getAddress() {
        return address;
    }

    public int getYearOfEmployment() {
        return yearOfEmployment;
    }

    public int getMonthOfEmployment() {
        return monthOfEmployment;
    }

    public int getWorkExperience() {
        int currentMonth = 2;
        int currentYear = 2018;
        return currentMonth >= monthOfEmployment ? currentYear - yearOfEmployment : currentYear - yearOfEmployment - 1;
    }

}

class Database {
    private List<Employee> employees = new ArrayList<>();

    public Database() {
        addEmployee("Ivanov", "Petr", "Sidorovich", "Baltiyskyi, 4", 2, 2009);
        addEmployee("Petrov", "Sidor", "Ivanovich", "Molostovyh, 53", 3, 2012);
        addEmployee("Sidorov", "Ivan", "Petrovich", "Onejskaya, 12", 4, 2013);
        addEmployee("Mankirov", "Oleg", "Kachuevskaya, 213", 5, 2014);
        addEmployee("Ivanov", "Petr", "Oktyabrskaya, 645", 6, 2010);
        addEmployee("Petrov", "Ivan", "Solnechnaya, 312", 7, 2011);
        addEmployee("Ivanov", "Sidor", "Igorevich", "Baker Street, 42", 8, 2012);
        addEmployee("Sidorov", "Feofan", "Lesnaya, 67", 9, 2013);
        addEmployee("Kuznecov", "Alexey", "Leninskiy prospekt, 123", 10, 2014);
        addEmployee("Smirnova", "Ekaterina", "Naberejnaya, 54", 11, 2015);
        addEmployee("Popov", "Vladimir", "Malahitovaya, 123", 12, 2015);
        addEmployee("Sokolov", "Evgeniy", "Vasilievich", "Bashova, 12", 1, 2014);
        addEmployee("Lebedev", "Konstantin", "Romanovich", "Kaluzhskaya, 76", 2, 2013);
        addEmployee("Sergeev", "Roman", "Artemovich", "Kaluzhskaya, 76", 2, 2013);
        addEmployee("Kozlov", "Artem", "Igorevich", "Rostokinskaya, 13", 3, 2015);
        addEmployee("Volkov", "Alexander", "Fedorovich", "Kosmonavtov, 56", 4, 2010);
        addEmployee("Zaitsev", "Sergey", "Anatolievich", "Mitrofanovskaya, 23", 5, 2011);
        addEmployee("Zaitseva", "Evgeniya", "Anatolievna", "Profsoyuznaya, 54", 6, 2012);
        addEmployee("Mihailova", "Lubov", "Vitalievna", "Narodnogo Opolcheniya, 432", 7, 2017);
        addEmployee("Belov", "Sergey", "Sergeevich", "Lubyanka, 26", 8, 2015);
    }

    private void addEmployee(String surname, String name, String patronymic,
                             String address, int monthOfEmployment, int yearOfEmployment) {
        employees.add(new Employee(surname, name, patronymic, address, monthOfEmployment, yearOfEmployment));
    }

    private void addEmployee(String surname, String name, String address, int monthOfEmployment, int yearOfEmployment) {
        employees.add(new Employee(surname, name, address, monthOfEmployment, yearOfEmployment));
    }

    List<Employee> getEmployeesName(String s) {
        List<Employee> employeesName = new ArrayList<>();
        for (Employee employee : employees) {
            String f = employee.getSurname() + employee.getName() + employee.getPatronymic();
            if (f.toLowerCase().contains(s.toLowerCase())) {
                employeesName.add(employee);
            }
        }
        return employeesName;
    }

    List<Employee> getEmployeesDate(int n) {
        List<Employee> employeesDate = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.getWorkExperience() >= n) {
                employeesDate.add(employee);
            }
        }
        return employeesDate;
    }
}