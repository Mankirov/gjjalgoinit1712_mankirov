package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N056.isPrimeNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N056Test {
    public static void main(String[] args) {
        testIsPrimeNumberIsTrue();
        testIsPrimeNumberIsFalse();
    }

    static void testIsPrimeNumberIsTrue() {
        assertEquals("TaskCh10N056Test.testIsPrimeNumberIsTrue", true, isPrimeNumber(11));
    }

    static void testIsPrimeNumberIsFalse() {
        assertEquals("TaskCh10N056Test.testIsPrimeNumberIsFalse", false, isPrimeNumber(123));
    }
}
