package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh10N052 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        System.out.println(recursionReverse(num, 0));
    }

    static int recursionReverse(int num, int reverseNum) {
        if (num % 10 == num) {
            return reverseNum + num;
        }
        return recursionReverse(num / 10, (reverseNum + num % 10) * 10);
    }
}
