package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

import static java.lang.Math.random;

public class TaskCh12N063 {
    final static int numberOfParallelsInSchool = 11;
    final static int numberOfClassesInParallel = 4;
    final static int minimumNumberOfStudentsPerClass = 15;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] school = new int[numberOfParallelsInSchool][numberOfClassesInParallel];
        for (int i = 0; i < school.length; i++) {
            for (int j = 0; j < school[i].length; j++) {
                school[i][j] = minimumNumberOfStudentsPerClass + (int) (random() * n);
                System.out.print(school[i][j] + " ");
            }
            System.out.println();
        }
        int[] result = determineAverageNumberInParallels(school);
        for (int i = 0; i < school.length; i++) {
            System.out.println("Average number of students in " + (i + 1) + " grade is " + result[i]);
        }
    }

    static int[] determineAverageNumberInParallels(int[][] school) {
        int[] averageNumberInParallel = new int[numberOfParallelsInSchool];
        for (int i = 0; i < school.length; i++) {
            int sum = 0;
            for (int j = 0; j < school[i].length; j++) {
                sum += school[i][j];
            }
            averageNumberInParallel[i] = sum / school[i].length;
        }
        return averageNumberInParallel;
    }
}
