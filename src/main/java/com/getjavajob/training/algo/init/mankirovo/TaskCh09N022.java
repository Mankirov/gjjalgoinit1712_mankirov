package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh09N022 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String result = isFirstHalf(sc.nextLine());
        System.out.println(result);
    }

    static String isFirstHalf(String word) {
        if (word.length() % 2 != 0) {
            return "Error! the number of letters must be even";
        }
        return word.substring(0, word.length() / 2);
    }
}
