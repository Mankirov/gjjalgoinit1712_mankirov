package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N042.recCalculateNumberInPower;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N042Test {
    public static void main(String[] args) {
        testRecCalculateNumberInPower();
    }

    static void testRecCalculateNumberInPower() {
        assertEquals("TaskCh10N042Test.testRecCalculateNumberInPower", 1024, recCalculateNumberInPower(2, 10));
    }
}
