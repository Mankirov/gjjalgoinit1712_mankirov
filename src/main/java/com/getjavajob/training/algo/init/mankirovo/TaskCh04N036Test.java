package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh04N036.determineColorSignal;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N036Test {
    public static void main(String[] args) {
        testDetermineColorSignalRed();
        testDetermineColorSignalGreen();
    }

    static void testDetermineColorSignalRed() {
        assertEquals("TaskCh04N036Test.testDetermineColorSignalRed", "Red", determineColorSignal(3));
    }

    static void testDetermineColorSignalGreen() {
        assertEquals("TaskCh04N036Test.testDetermineColorSignalGreen", "Green", determineColorSignal(5));
    }
}
