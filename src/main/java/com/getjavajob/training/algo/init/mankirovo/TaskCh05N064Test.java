package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh05N064.determineRegionPopulation;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N064Test {
    public static void main(String[] args) {
        testDetermineRegionPopulation();
    }

    static void testDetermineRegionPopulation() {
        double[] districtArea = {12.0, 15.0, 18.0, 12.0, 15.0, 18.0, 12.0, 15.0, 18.0, 12.0, 15.0, 18.0};
        double[] districtPopulation = {24.0, 30.0, 36.0, 26.5, 30.0, 33.5, 20.0, 30.0, 40.0, 22.5, 30.0, 37.5};
        assertEquals("TestCh05N064Test.TestDetermineRegionPopulation",
                2.0, determineRegionPopulation(districtArea, districtPopulation));
    }
}
