package com.getjavajob.training.algo.init.mankirovo;

public class TaskCh03N029 {
    public static void main(String[] args) {
        getResultTaskA(3, 5);
        getResultTaskB(10, 20);
        getResultTaskV(0, 20);
        getResultTaskG(-5, -6, -7);
        getResultTaskD(14, 15, 16);
        getResultTaskE(5, 101, 98);
    }

    /**
     * solution of the task A
     *
     * @return true when each of the numbers X and Y is odd
     */
    static boolean getResultTaskA(int x, int y) {
        return x % 2 != 0 & y % 2 != 0;
    }

    /**
     * solution of the task b
     *
     * @return true only one of the numbers X and Y is less than twenty
     */
    static boolean getResultTaskB(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * solution of the task v
     *
     * @return true when at least one of the numbers X and Y is zero
     */
    static boolean getResultTaskV(int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * solution of the task G
     *
     * @return true when each of the numbers X, Y, Z is negative
     */
    static boolean getResultTaskG(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * solution of the task D
     *
     * @return return true, when only one of the numbers X, Y and Z is a multiple of five
     */
    static boolean getResultTaskD(int x, int y, int z) {
        boolean a = x % 5 == 0;
        boolean b = y % 5 == 0;
        boolean c = z % 5 == 0;
        return !(a && b && c) && (a ^ b ^ c);
    }

    /**
     * solution of the task e
     *
     * @return true when at least one of the numbers X, Y, Z is greater than one hundred
     */
    static boolean getResultTaskE(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}
