package com.getjavajob.training.algo.init.mankirovo;

public class TaskCh10N053 {
    public static void main(String[] args) {
        int[] array = {0, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] result = recReverseArray(array, array.length);
        for (int aResult : result) {
            System.out.print(aResult);
        }

    }

    static int[] recReverseArray(int[] array, int numElements) {
        if (numElements == array.length / 2) {
            return array;
        }
        int y = array[array.length - numElements];
        array[array.length - numElements] = array[numElements - 1];
        array[numElements - 1] = y;
        return recReverseArray(array, numElements - 1);
    }
}
