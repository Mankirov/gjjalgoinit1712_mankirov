package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh09N042 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String result = makeReverse(sc.nextLine());
        System.out.println(result);
    }

    static String makeReverse(String word) {
        StringBuilder wordSb = new StringBuilder(word);
        return wordSb.reverse().toString();
    }
}
