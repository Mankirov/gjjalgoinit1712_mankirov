package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh02N043.findResiduesProduct;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N043Test {
    public static void main(String[] args) {
        testFindResiduesProductTrueFirstBySecond();
        testFindResiduesProductTrueSecondByFirst();
        testFindResiduesProductFalse();
    }

    static void testFindResiduesProductTrueFirstBySecond() {
        assertEquals("TaskCh02N043Test.testFindResiduesProductTrueFirstBySecond", 1, findResiduesProduct(6, 2));
    }

    static void testFindResiduesProductTrueSecondByFirst() {
        assertEquals("TaskCh02N043Test.testFindResiduesProductTrueSecondByFirst", 1, findResiduesProduct(3, 6));
    }

    static void testFindResiduesProductFalse() {
        assertEquals("TaskCh02N043Test.testFindResiduesProductFalse", 3, findResiduesProduct(6, 4));
    }
}