package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        int result = swapSecondAndThirdDigits(input);
        System.out.println(result);
    }

    static int swapSecondAndThirdDigits(int input) {
        if (input <= 100 || input >= 999) {
            System.out.println("the number must have three digits");
            return 0;
        }
        int firsDigit = input / 100;
        int secondDigit = input / 10 % 10;
        int thirdDigit = input % 10;
        return firsDigit * 100 + thirdDigit * 10 + secondDigit;
    }
}
