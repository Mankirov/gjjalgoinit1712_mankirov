package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh10N056 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        boolean result = isPrimeNumber(number);
        System.out.println(result);
    }

    static boolean isPrimeNumber(int number, int halfNumber) {
        if (halfNumber == 1) {
            return true;
        }else if (number % halfNumber == 0) {
            return false;
        }
        return isPrimeNumber(number, halfNumber - 1);

    }

    static boolean isPrimeNumber(int number) {
        return isPrimeNumber(number, number / 2);
    }
}
