package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh09N185 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String result = findExtraBraces(sc.nextLine());
        System.out.println(result);
    }

    static String findExtraBraces(String arithExpression) {
        int count = 0;
        String[] arithSigns = arithExpression.split("");
        String answer = "";
        for (int i = 0; i < arithSigns.length; i++) {
            if (arithSigns[i].equals("(")) {
                count++;
            } else if (arithSigns[i].equals(")")) {
                count--;
            }
            if (count < 0) {
                answer += "the first extra ) has an index " + i;
                break;
            } else if (count > 0 && i == arithSigns.length - 1) {
                answer += count + " extra (";
            }
        }
        return answer;
    }
}
