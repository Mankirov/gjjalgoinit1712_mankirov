package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N043.calculateNumberDigits;
import static com.getjavajob.training.algo.init.mankirovo.TaskCh10N043.calculateSumDigits;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N043Test {
    public static void main(String[] args) {
        testCalculateSumDigits();
        testCalculateNumberDigits();
    }

    static void testCalculateSumDigits() {
        assertEquals("TaskCh10N043Test.testCalculateSumDigits", 15, calculateSumDigits(12345));
    }

    static void testCalculateNumberDigits() {
        assertEquals("TaskCh10N043Test.testCalculateNumberDigits", 5, calculateNumberDigits(12345));
    }
}
