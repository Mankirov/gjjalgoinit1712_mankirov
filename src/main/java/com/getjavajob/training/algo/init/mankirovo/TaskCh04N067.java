package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

public class TaskCh04N067 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        String result = isWorkOrWeek(k);
        System.out.println(result);
    }

    static String isWorkOrWeek(int k) {
        return k % 7 > 0 && k % 7 < 6 ? "Workday" : "Weekday";
    }
}
