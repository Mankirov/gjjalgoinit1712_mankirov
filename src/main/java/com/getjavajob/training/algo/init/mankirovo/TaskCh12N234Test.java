package com.getjavajob.training.algo.init.mankirovo;

import static com.getjavajob.training.algo.init.mankirovo.TaskCh12N234.deleteColumnS;
import static com.getjavajob.training.algo.init.mankirovo.TaskCh12N234.deleteStringK;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N234Test {
    public static void main(String[] args) {
        testDeleteStringK();
        testDeleteColumnS();
    }

    static void testDeleteStringK() {
        assertEquals("TaskCh12N234Test.testDeleteStringK", new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {13, 12, 11, 10, 9},
                {0, 0, 0, 0, 0}
        }, deleteStringK(new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        }, 3));
    }

    static void testDeleteColumnS() {
        assertEquals("TaskCh12N234Test.testDeleteColumnS", new int[][]{
                        {1, 2, 3, 5, 0},
                        {16, 17, 18, 6, 0},
                        {15, 24, 25, 7, 0},
                        {14, 23, 22, 8, 0},
                        {13, 12, 11, 9, 0}
                }, deleteColumnS(new int[][]{
                        {1, 2, 3, 4, 5},
                        {16, 17, 18, 19, 6},
                        {15, 24, 25, 20, 7},
                        {14, 23, 22, 21, 8},
                        {13, 12, 11, 10, 9}
                }, 3)
        );
    }
}
