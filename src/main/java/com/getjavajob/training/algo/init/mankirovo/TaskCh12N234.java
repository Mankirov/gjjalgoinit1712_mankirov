package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

import static java.lang.Math.random;

public class TaskCh12N234 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();// string
        int s = sc.nextInt();// column
        int[][] array = new int[n][n];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = (int) (random() * n);
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        int[][] resultK = deleteStringK(array, k);
        int[][] resultS = deleteColumnS(array, s);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(resultK[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(resultS[i][j] + " ");
            }
            System.out.println();
        }
    }

    static int[][] deleteStringK(int[][] m, int k) {
        for (int i = k; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                if (i < m.length - 1) {
                    m[i][j] = m[i + 1][j];
                } else {
                    m[i][j] = 0;
                }
            }
        }
        return m;
    }

    static int[][] deleteColumnS(int[][] m, int s) {
        for (int i = 0; i < m.length; i++) {
            for (int j = s; j < m[i].length; j++) {
                if (j < m[i].length - 1) {
                    m[i][j] = m[i][j + 1];
                } else {
                    m[i][j] = 0;
                }
            }
        }
        return m;
    }
}
