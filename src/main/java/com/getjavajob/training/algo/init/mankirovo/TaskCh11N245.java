package com.getjavajob.training.algo.init.mankirovo;

import java.util.Scanner;

import static java.lang.Math.random;

public class TaskCh11N245 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] oldArray = new int[n];
        for (int i = 0; i < oldArray.length; i++) {
            oldArray[i] = (int) (random() * n - n / 2);
            System.out.print(oldArray[i] + " ");
        }
        int[] result = separateNegativeAndPositive(oldArray);
        for (int aResult : result) {
            System.out.print(aResult + " ");
        }
    }

    static int[] separateNegativeAndPositive(int[] oldArray) {
        int[] newArray = new int[oldArray.length];
        for (int i = 0; i < oldArray.length; i++) {
            if (oldArray[i] >= 0) {
                newArray[i] = oldArray[i];
            } else {
                addNegative(newArray, oldArray[i]);
            }
        }
        return newArray;
    }

    static void addNegative(int[] newArray, int oldArray) {
        for (int i = 0; i < newArray.length; i++) {
            if (newArray[i] >= 0) {
                for (int y = newArray.length - 1; y > i; y--) {
                    newArray[y] = newArray[y - 1];
                }
                newArray[i] = oldArray;
                break;
            }
        }
    }
}
